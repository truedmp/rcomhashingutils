# RcomHashingUtils

Utility to compute hashing based on ssoid with option to rotate hashing every
X passing weeks. 

## Change Log

0.1.0

- Modify hashing algorithm to support hash string (DMPREC-2493)

0.0.1

- First version with utils extracted from UFS.
