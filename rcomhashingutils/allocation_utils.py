#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 16:29:19 2019

@author: ktp
"""

import os
import time
import hashlib
from typing import List
from rcomloggingutils.applog_utils import AppLogger


LOGGER_LEVEL = int(os.getenv("LOGGER_LEVEL", "20"))         # DEBUG = 10, WARNING = 30
APPLOGGER_LEVEL = int(os.getenv("APPLOGGER_LEVEL", "30"))   # DEBUG = 10, WARNING = 30


_logger = AppLogger(name=__name__, applog_level=APPLOGGER_LEVEL, log_level=LOGGER_LEVEL)


def _get_hash(values: List[str]) -> int:
    """Get hashing value from input values

    Args:
        values: List of string to be hashed
    
    Returns:
        Hashing number
    """
    m = hashlib.md5()
    for value in values:
        _logger.debug(f"Get hashing value for {value}")
        m.update(str(value).encode('utf-8'))
    # hash str to hex
    hash = m.hexdigest()
    # convert hex to decimal
    hash_int = int(hash, 16)
    return hash_int


def hash_ssoid(ssoid, hash_string="", period=None, day_of_year=None):
    """Get hashing value for ssoid

    Args:
        ssoid: Single signed-on ID
        hash_string: other hashing key, for example, experiment name.
        period: days to re-allocate the group. If not given, the hash is 
                static for each ssoid.
        day_of_year: If not given, use the current day of year. Otherwise
                use the given number as day_of_year. This is ignored if 
                period is not given.
    
    Returns:
        An integer (as string) which is computed by
        hash(ssoid, hashing_string) + hash(day_of_year // period)
    """

    _logger.debug("hashing ssoid to select model based on day_of_year and period")
    hash_ssoid = _get_hash(values=[ssoid, hash_string])
    hash_days = 0
    
    if period:
        if (not day_of_year) or (not isinstance(day_of_year, int)):
            day_of_year = int(time.strftime("%-j"))
        
        hash_days = _get_hash(values=[(day_of_year // period)])

    hash_str = str(hash_ssoid + hash_days)
    return hash_str
    
    
    
    