from setuptools import setup, find_packages

REQUIRED_PACKAGES = [
    'rcomloggingutils==1.1.1'
]

setup(name='rcomhashingutils',
      version='0.1.0',
      packages=find_packages(exclude=('tests',)),
      url='https://bitbucket.org/truedmp/rcomhashingutils.git',
      include_package_data=True,
      description='Rcom common utils for hashing ssoid',
      author='Ktawut T.Pijarn',
      author_email='ktawut.tap@truedigital.com',
      install_requires=REQUIRED_PACKAGES,
      zip_safe=False)

