#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 16:38:01 2019

@author: ktp
"""

import unittest as ut
import scipy.stats as ss
import numpy as np
import unittest.mock as mock
import rcomhashingutils.allocation_utils as alu


class AllocationUtilsTest(ut.TestCase):
    
    
    @classmethod
    def setUpClass(cls):
        pass


    @classmethod
    def tearDownClass(cls):
        pass
    
    
    def test_get_hash_has_uniform_distribution(self):
        
        count = 10000
        last_digits = []
        
        for i in range(count):
            last_digit = alu._get_hash([i]) % 10
            last_digits.append(last_digit)
            
        # Testing for closeness to discrete uniform distribution using chi-square
        # https://stackoverflow.com/questions/22392562/how-can-check-the-distribution-of-a-variable-in-python
            
        # by default, chisquare tests null hypothesis that observed data has 
        # uniform distribution across all discrete categories
        digit_freq = (last_digits==np.arange(10)[...,np.newaxis]).sum(axis=1)*1./len(last_digits)
        _chi, p_value = ss.chisquare(digit_freq)
        
        # p_value > 0.05 means the null hypo is true (that it's indeed uniform)
        self.assertTrue(p_value > 0.05)
    
    def test_get_hash_has_uniform_distribution_when_adding_hash_string(self):
        
        count = 10000
        last_digits = []
        
        for i in range(count):
            last_digit = alu._get_hash([i, 'hash_string']) % 10
            last_digits.append(last_digit)
            
        # Testing for closeness to discrete uniform distribution using chi-square
        # https://stackoverflow.com/questions/22392562/how-can-check-the-distribution-of-a-variable-in-python
            
        # by default, chisquare tests null hypothesis that observed data has 
        # uniform distribution across all discrete categories
        digit_freq = (last_digits==np.arange(10)[...,np.newaxis]).sum(axis=1)*1./len(last_digits)
        _chi, p_value = ss.chisquare(digit_freq)
        
        # p_value > 0.01 means the null hypo is true (that it's indeed uniform)
        self.assertTrue(p_value > 0.01)
    
    @mock.patch("time.strftime")
    @mock.patch("rcomhashingutils.allocation_utils._get_hash")
    def test_hash_ssoid_1(self, hash_mock, time_mock):
        
        ssoid ="12345"
        period = 30
        day_of_year = 90
        exp_hash_ssoid = "6912"
        time_mock.return_value = day_of_year
        hash_mock.side_effect = [1234, 5678]
        hash_ssoid = alu.hash_ssoid(ssoid=ssoid, period=period)
        self.assertEqual(hash_ssoid,exp_hash_ssoid)
        self.assertEqual(type(hash_ssoid), type(exp_hash_ssoid))
        calls = [mock.call(values=[ssoid, '']), mock.call(values=[(day_of_year//period)])]
        alu._get_hash.assert_has_calls(calls=calls)
    

    @mock.patch("time.strftime")
    def test_hash_ssoid_2(self, time_mock):

        ssoid = "12345"
        period = 30
        day_of_year = 90
        exp_hash_ssoid = "488203512528943645556797689812572178798"
        time_mock.return_value = day_of_year
        hash_ssoid = alu.hash_ssoid(ssoid=ssoid, period=period)
        self.assertEqual(hash_ssoid, exp_hash_ssoid)
        self.assertEqual(type(hash_ssoid), type(exp_hash_ssoid))

        period = 31
        hash_ssoid = alu.hash_ssoid(ssoid=ssoid, period=period)
        self.assertNotEqual(hash_ssoid, exp_hash_ssoid)

        period = 30
        hash_ssoid = alu.hash_ssoid(ssoid=ssoid, period=period)
        self.assertEqual(hash_ssoid, exp_hash_ssoid)

        day_of_year = 120
        time_mock.return_value = day_of_year
        hash_ssoid = alu.hash_ssoid(ssoid=ssoid, period=period)
        self.assertNotEqual(hash_ssoid, exp_hash_ssoid)
        
        
    @mock.patch("time.strftime")
    def test_hash_ssoid_3(self, time_mock):
        
        # test overriding day_of_year
        ssoid = "12345"
        period = 30
        day_of_year = 90
        time_mock.return_value = 1
        hash_ssoid_1 = alu.hash_ssoid(ssoid=ssoid, period=period, day_of_year=day_of_year)
        
        time_mock.return_value = 10
        hash_ssoid_2 = alu.hash_ssoid(ssoid=ssoid, period=period, day_of_year=day_of_year)
        
        time_mock.return_value = 100
        hash_ssoid_3 = alu.hash_ssoid(ssoid=ssoid, period=period, day_of_year=day_of_year)
        
        self.assertEqual(hash_ssoid_1, hash_ssoid_2)
        self.assertEqual(hash_ssoid_2, hash_ssoid_3)
        
    def test_get_hash_value_should_return_the_same_hash_when_value_is_empty(self):
        ssoid = "12345"
        hashed_only_ssoid = alu._get_hash([ssoid])
        hashed_with_empty_string = alu._get_hash([ssoid, ''])
        self.assertEqual(hashed_only_ssoid, hashed_with_empty_string)

    def test_hash_ssoid_should_return_different_hashed_when_hash_string_is_different(self):
        ssoid = "12345"
        actual_1 = alu.hash_ssoid(ssoid, hash_string="experiment_1")
        actual_2 = alu.hash_ssoid(ssoid, hash_string="experiment_2")
        self.assertNotEqual(actual_2, actual_1)

        
if __name__ == '__main__':
    ut.main()